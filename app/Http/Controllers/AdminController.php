<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Rules\AdminLoginRule;
use App\Rules\AdminPasswordRule;

class AdminController extends Controller
{
    public function Login(Request $request)
    {
      $validator = Validator::make($request->all(),
      [
        'login' => ["required", new AdminLoginRule],
        'password' => ["required", new AdminPasswordRule],
      ]);

      if ($validator->fails()) {
        return redirect('/getadmin')
                    ->withErrors($validator);
      }
      else
      {
        return redirect()->route('index-admin');
      }
    }
}
