<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\BlogEditorModel;
use App\Filesystem\PostFile;

class BlogController extends Controller
{
    public function submit(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'theme' => 'required',
        'content' => 'required'
      ]);

      //Validator fail handling
      if ($validator->fails())
      {
        return redirect()
                  ->route("blog-editor")
                  ->withErrors($validator);
      }

      $editorModel = new BlogEditorModel();

      $editorModel->theme = $request->input('theme');
      $editorModel->content = $request->input('content');

      $editorModel->imagename =
            (isset($_FILES["imageFile"])) ? $_FILES["imageFile"]["name"] : NULL;

      $editorModel->save();

      return redirect()->route("blog-editor");
    }

    public function LoadFile(Request $request)
    {
      if(isset($_FILES["postFile"]))
      {
        $editorModel = new BlogEditorModel();
        $fileName = $_FILES["postFile"]["name"];
        $editorModel->SaveDataFileToServer("postFile");

        $file = new PostFile($fileName);
        $posts = $file->GetAllPosts();

        foreach ($posts as $post)
        {
          $editorModel = new BlogEditorModel();
          $editorModel->theme = $post["title"];
          $editorModel->content = $post["message"];
          $editorModel->imagename = $post["imagename"];

          $editorModel->save();
        }

        return redirect()->route("blog");
      }
    }

    public function GetData()
    {
      $data = BlogEditorModel::all();
      return view("blog", ['data' => $data]);
    }
}
