<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogEditorModel extends Model
{
  public function save($options = array())
  {
    parent::save($options);
    $this->SaveDataImageToServer();
  }

  /*---------------------------------------------------*/

  private function SaveDataImageToServer()
  {
    if (isset($_FILES["imageFile"]))
    {
      $source = $_FILES["imageFile"]["tmp_name"];
      $dest = "img/posts/".$_FILES["imageFile"]["name"];
      move_uploaded_file($source, $dest);
    }
  }

  /*---------------------------------------------------*/

  public function SaveDataFileToServer($fileName)
  {
    $source = $_FILES[$fileName]["tmp_name"];
    $dest = "../storage/app/posts/".$_FILES[$fileName]["name"];
    move_uploaded_file($source, $dest);
  }
}
