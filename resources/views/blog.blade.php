@extends('layouts.user')

@section('title-name')
  Our Blog
@endsection

@section('content')
<!-- <div class="formBox" id = "commentBox">
  <div class="closeFormHeader">
    <div class="closeFormButton"></div>
  </div>
  <div class="formBoxContent">
    <form action="/web/comment/save" method = "POST" name = "commentForm" id = "commentForm">
      <div class="inputBlock">
        <h3>What do you think about this post?</h3>
        <textarea name="commentText" rows="8" cols="80" placeholder="Write your comment here..."></textarea>
      </div>
      <div class="form-buttons">
        <input type="submit" class = "helper-button commentSubmit" value = "Login" form = "commentForm">
      </div>
    </form>
  </div>
</div> -->

<div class = "queue center">
  <div class="limitation">
    @foreach($data as $post)
      <div class="blogPost">
        <h1>{{ $post->theme }}</h1>
        <span>{{ $post->updated_at }}</span>
        <p>{{ $post->content }}</p>
        <img src = <?php if(isset($post->imagename)) echo "img/posts/".$post->imagename; ?>>

      </div>
    @endforeach

  </div>
</div>
@endsection
