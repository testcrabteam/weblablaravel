@extends('layouts.admin')

@section('title-name')
  Our Blog editor
@endsection

@section('content')
<div id="form">
  <div class="limitation">
    <div class="form-header">
      <h2>Feedback</h2>
      <p>You can write the post about self!</p>
      <div class="spacer">
        <img src="/img/spacer.png" alt="spacer">
      </div>
    </div>
    <div class="feedback">
      <div class="feedback-form center">
        <form action="{{ route('post-submit') }}" method = "POST" name = "form" id = "form" enctype="multipart/form-data">
          @csrf
           <div class="inputBlock">
             <input type="text" placeholder = "Theme" name = "theme" autocomplete="off" required>
           </div>
           <div class="inputBlock">
             <textarea placeholder = "Message" name = "content"
               class = <?php if ($errors->has('content')): echo "failedInput"; ?>

               <?php endif; ?>></textarea>
             <p>{{ $errors->first('content') }}</p>
           </div>
          <div class="spaceBetween">
            <input type="submit" class = "helper-button" value = "Submit">
            <input type="reset" class = "helper-button" value = "Reset">
            <input type="file" value = "Your image" name = "imageFile" id = "imageFile">
            <label for="imageFile" class = "helper-button">
              <p>Load your image!</p>
            </label>
          </div>
        </form>
      </div>
    </div>
    <!-- <div class="feedback">
      <div class="feedback-form center">
        <form action="/web/blogeditor/sendFile" method="post" enctype="multipart/form-data" id = "form">
          <div class="spaceBetween">
            <input type="file" value = "Your posts" name = "postFile" id = "postFile">
            <label for="postFile" class = "helper-button">
              <p>Load your posts file!</p>
            </label>
            <input type="submit" class = "helper-button" value = "Submit">
          </div>
        </form>
      </div>
      </div> -->
  </div>
</div>
@endsection
