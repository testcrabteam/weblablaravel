@extends('layouts.admin')

@section('title-name')
  Blog files loader
@endsection

@section('content')
<div id="form">
  <div class="limitation">
    <div class="form-header">
      <h2>Feedback</h2>
      <p>You can write the post about self!</p>
      <div class="spacer">
        <img src="/img/spacer.png" alt="spacer">
      </div>
    </div>
    <div class="feedback">
      <div class="feedback-form center">
        <form action="{{ route('post-load') }}" method="post" enctype="multipart/form-data" id = "form">
          @csrf
          <div class="spaceBetween">
            <input type="file" value = "Your posts" name = "postFile" id = "postFile">
            <label for="postFile" class = "helper-button">
              <p>Load your posts file!</p>
            </label>
            <input type="submit" class = "helper-button" value = "Submit">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
