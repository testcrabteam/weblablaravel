@extends('layouts.user')

@section('title-name')
  Admin form
@endsection

@section('content')
<div id="form">
  <div class="limitation">
    <div class="form-header">
      <h2>Come in!</h2>
      <p>Site can't work without You!</p>
      <div class="spacer">
        <img src="img/spacer.png" alt="spacer">
      </div>
    </div>
    <div class="feedback">
      <div class="feedback-form alone">
        <form action="{{ route('admin-login') }}" method = "POST" name = "loginForm" id = "loginForm">
          @csrf

          <div class="inputBlock">
            <input type="text"placeholder = "Login" name = "login" autocomplete="off"
            class = <?php if ($errors->has('login')): echo "failedInput"; ?>

            <?php endif; ?>>
            <p>{{ $errors->first('login') }}</p>
          </div>
          <div class="inputBlock">
            <input type="password" placeholder = "Password" name = "password" autocomplete="off"
            class = <?php if ($errors->has('password')): echo "failedInput";?>

            <?php endif; ?>>
            <p>{{ $errors->first('password') }}</p>
          </div>
           <div class="form-buttons">
             <input type="submit" class = "helper-button" value = "Submit">
             <input type="reset" class = "helper-button" value = "Reset">
           </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
