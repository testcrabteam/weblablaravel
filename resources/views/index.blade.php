@extends('layouts.user')

@section('title-name')
  Welcome!
@endsection

@section('content')
<div id="competence">
  <div class="limitation">
    <div class="competence-item">
      <div class="competence-bg">
        <div class="competence-bg-border">
          <img src="img/compentence1.png" alt="bg">
        </div>
        <h3>Дизайн</h3>
      </div>
      <div class="spacer"></div>
      <p>
        Я черпал вдохновения для создания дизайна из psd-макета одного сайта...
        Весь материал и решения черпал оттуда, ибо фиг вам, а не "придумывать свое")
      </p>
    </div>
    <div class="competence-item">
      <div class="competence-bg">
        <div class="competence-bg-border">
          <img src="img/compentence2.png" alt="bg">
        </div>
        <h3>Технология</h3>
      </div>
      <div class="spacer"></div>
      <p>
        Сайт создан с помощью 3-х основных технологий:
        html, css, гребаное терпение.
        </p>
    </div>
    <div class="competence-item">
      <div class="competence-bg">
        <div class="competence-bg-border">
          <img src="img/compentence3.png" alt="bg">
        </div>
        <h3>Перспективы</h3>
      </div>
      <div class="spacer"></div>
      <p>
        Сайт является площадкой для дальнейших лабораторных работ по вебу.
        Возможна разработка нестандартных интересных скриптов.
      </p>
    </div>
    <div class="competence-item">
      <div class="competence-bg">
        <div class="competence-bg-border">
          <img src="img/compentence4.png" alt="bg">
        </div>
        <h3>Поддержка</h3>
      </div>
      <div class="spacer"></div>
      <p>
        Проект поддерживается, пока к нему есть интерес.
        Мой) И... Может еще кого-нибудь.
      </p>
    </div>
  </div>
</div>
<div id="team">
  <div id="team-graphic">
    <div id="team-header">
      <h2>My dream Team</h2>
      <h4>С кем я готов работать всегда и везде</h4>
      <div class="spacer">
        <img src="img/spacer.png" alt="spacer">
      </div>
    </div>
    <div id="team-content">
      <div class="limitation">
        <div class="teammate">
          <div class="teammate-photo nadya"></div>
          <div class="vert-spacer"></div>
          <div class="teammate-info">
            <p>Жукова Надя</p>
            <p>Дизайнер</p>
          </div>
        </div>
        <div class="teammate">
          <div class="teammate-photo stroganov"></div>
          <div class="vert-spacer"></div>
          <div class="teammate-info">
            <p>Строганов Виктор</p>
            <p>Специалист по рефакторингу</p>
          </div>
        </div>
        <div class="teammate">
          <div class="teammate-photo zab"></div>
          <div class="vert-spacer"></div>
          <div class="teammate-info">
            <p>Забаштанский Андрей</p>
            <p>Бойтесь этого человека...</p>
          </div>
        </div>
        <div class="teammate">
          <div class="teammate-photo misterclass"></div>
          <div class="vert-spacer"></div>
          <div class="teammate-info">
            <p>Rafail</p>
            <p>Самый лучший из армян</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
