<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title-name')</title>
  <link rel="icon" href="img/icon.png">
  <link rel="stylesheet" type = "text/css" href="/css/style.css">
</head>
<body>
  <div id = "root feedback-main">
    <header>
      <div class="limitation">
        <div id="header-top">
          <div id="header-logo">
            <a href="index.html"><img src="/img/logo.png" alt = "Logo"></a>
          </div>
          <nav>
            <ul>
              <li><a href = "{{ route('blog-loader') }}">Blog Loader</a></li>
              <li><a href = "{{ route('blog-editor') }}">Blog Editor</a></li>
              <li><a href = "{{ route('index-admin') }}">Home</a></li>
            </ul>
          </nav>
        </div>
        <div id="header-description">
          <h2>What's wrong?</h2>
          <h1>Welcome, MisterClass!</h1>
          <div class="spacer">
            <img src="/img/spacer.png" alt="spacer">
          </div>
          <div class="text-limiter">
            <p>
              Greeting! Let's fix bugs!
            </p>
          </div>
          <div class="helper-buttons">
            <div class="helper-button">
              <span>Get started now</span>
            </div>
            <div class="helper-button">
              <span>Learn more</span>
            </div>
          </div>
        </div>
        <div id="header-pivot">
          <div id="header-pivot-body">
            <img src="/img/pivot.png" alt="piv">
          </div>
        </div>
      </div>
    </header>
    <main>
      @yield('content');
    </main>
    <footer>
      <div class="limitation">
        <div id="socials">
          <a href="https://vk.com/misterclass"><img src="img/socicon1.png" alt="vk"></a>
        </div>
        <span>Mister Class &copy All Rights Reserved</span>
      </div>
    </footer>
  </div>
</body>
</html>
