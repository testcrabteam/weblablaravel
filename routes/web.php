<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name("index");

Route::get('/about', function () {
    return view('about');
})->name("about");

Route::get('/blog', "BlogController@GetData")->name("blog");

Route::get('/getadmin', function () {
    return view('getadmin');
});



Route::post('/getadmin/login', "AdminController@Login")->name("admin-login");

Route::get('/index/admin', function () {
    return view('indexAdmin');
})->name("index-admin");

Route::get('/blog/editor', function () {
    return view('blogEditor');
})->name("blog-editor");

Route::get('/blog/loader', function () {
    return view('blogLoader');
})->name("blog-loader");

Route::post('/blog/editor/submit', "BlogController@submit")->name("post-submit");

Route::post('/blog/load', "BlogController@LoadFile")->name("post-load");
